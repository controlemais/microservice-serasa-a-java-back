package br.com.serasaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoSerasaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoSerasaApplication.class, args);
	}

}

