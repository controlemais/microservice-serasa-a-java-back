package br.com.serasaa.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.serasaa.model.Cliente;
import br.com.serasaa.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente salvar(Cliente cliente) throws NoSuchAlgorithmException {

		return clienteRepository.save(cliente);
	}

	public Cliente atualizar(Integer codigo, Cliente cliente) {
		Cliente clienteBanco = buscarClientePeloCodigo(codigo);
		BeanUtils.copyProperties(cliente, clienteBanco, "clienteId");
		return clienteRepository.save(clienteBanco);
	}

	public Cliente buscarClientePeloCodigo(Integer codigo) {
		Cliente clienteBanco = clienteRepository.findById(codigo).orElse(null);
		if (clienteBanco == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return clienteBanco;
	}

	public List<Cliente> listarTodos() {
		return clienteRepository.findAll(new Sort(Sort.Direction.ASC, "nome"));
	}

	public void deletar(Integer codigo) {
		clienteRepository.deleteById(codigo);
	}

}
