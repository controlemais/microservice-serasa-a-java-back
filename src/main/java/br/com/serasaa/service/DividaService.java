package br.com.serasaa.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.serasaa.model.Divida;
import br.com.serasaa.repository.DividaRepository;

@Service
public class DividaService {

	@Autowired
	private DividaRepository dividaRepository;

	public Divida salvar(Divida divida) throws NoSuchAlgorithmException {
		return dividaRepository.save(divida);
	}

	public Divida atualizar(Integer codigo, Divida divida) {
		Divida dividaBanco = buscarDividaPeloCodigo(codigo);
		BeanUtils.copyProperties(divida, dividaBanco, "dividaId");
		return dividaRepository.save(dividaBanco);
	}

	public Divida buscarDividaPeloCodigo(Integer codigo) {
		Divida dividaBanco = dividaRepository.findById(codigo).orElse(null);
		if (dividaBanco == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return dividaBanco;
	}

	public List<Divida> listarTodos() {
		return dividaRepository.findAll(new Sort(Sort.Direction.ASC, "dividaId"));
	}

	public void deletar(Integer codigo) {
		dividaRepository.deleteById(codigo);
	}

}
