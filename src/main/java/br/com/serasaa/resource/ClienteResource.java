package br.com.serasaa.resource;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.serasaa.event.RecursoCriadoEvent;
import br.com.serasaa.model.Cliente;
import br.com.serasaa.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteResource {

	@Autowired
	private ApplicationEventPublisher publisher;
	@Autowired
	private ClienteService clienteService;

	@CrossOrigin
	@GetMapping
	public List<Cliente> listar() {
		return clienteService.listarTodos();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Cliente> cria(@Valid @RequestBody Cliente cliente, HttpServletResponse response) throws NoSuchAlgorithmException {
		Cliente clienteSalvo = clienteService.salvar(cliente);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, clienteSalvo.getClienteId()));

		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);
	}

	@CrossOrigin
	@PutMapping("{codigo}")
	public ResponseEntity<Cliente> atualizar(@PathVariable Integer codigo, @RequestBody Cliente cliente) {

		return ResponseEntity.ok(clienteService.atualizar(codigo, cliente));
	}

	@CrossOrigin
	@GetMapping("/{codigo}")
	public ResponseEntity<Cliente> buscarPeloCodigo(@PathVariable Integer codigo, HttpServletResponse response) {
		Cliente cliente = clienteService.buscarClientePeloCodigo(codigo);

		if (cliente == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(cliente);
	}

	@CrossOrigin
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer codigo) {
		clienteService.deletar(codigo);
	}
	
	@CrossOrigin
	public List<Cliente> listarTodos() {	
		return clienteService.listarTodos();
	}

}