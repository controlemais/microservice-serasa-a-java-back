package br.com.serasaa.resource;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.serasaa.event.RecursoCriadoEvent;
import br.com.serasaa.model.Divida;
import br.com.serasaa.service.DividaService;

@RestController
@RequestMapping("/divida")
public class DividaResource {

	@Autowired
	private ApplicationEventPublisher publisher;
	@Autowired
	private DividaService dividaService;

	@CrossOrigin
	@GetMapping
	public List<Divida> listar() {
		return dividaService.listarTodos();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Divida> cria(@Valid @RequestBody Divida divida, HttpServletResponse response) throws NoSuchAlgorithmException {
		Divida dividaSalvo = dividaService.salvar(divida);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, dividaSalvo.getDividaId()));

		return ResponseEntity.status(HttpStatus.CREATED).body(dividaSalvo);
	}

	@CrossOrigin
	@PutMapping("{codigo}")
	public ResponseEntity<Divida> atualizar(@PathVariable Integer codigo, @RequestBody Divida divida) {

		return ResponseEntity.ok(dividaService.atualizar(codigo, divida));
	}

	@CrossOrigin
	@GetMapping("/{codigo}")
	public ResponseEntity<Divida> buscarPeloCodigo(@PathVariable Integer codigo, HttpServletResponse response) {
		Divida divida = dividaService.buscarDividaPeloCodigo(codigo);

		if (divida == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(divida);
	}

	@CrossOrigin
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer codigo) {
		dividaService.deletar(codigo);
	}
	
	@CrossOrigin
	public List<Divida> listarTodos() {	
		return dividaService.listarTodos();
	}

}