package br.com.serasaa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serasaa.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

}
