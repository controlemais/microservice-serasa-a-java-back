package br.com.serasaa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.serasaa.model.Divida;

public interface DividaRepository extends JpaRepository<Divida, Integer> {
	
	@Query(value = "SELECT * from divida d where d.cliente_id = ?1 order by d.divida_id ", nativeQuery = true)
	public List<Divida> listarPendentePorUsuario(Integer usuarioId);
}
