CREATE TABLE public.cliente (
  cliente_id SERIAL NOT NULL,
  nome TEXT NOT NULL,
  cpf TEXT,
  endereco pg_catalog.jsonb,
  PRIMARY KEY(cliente_id)
) ;


CREATE TABLE public.divida (
  divida_id SERIAL NOT NULL,
  valor NUMERIC(17,2) NOT NULL,
  data DATE,
  descricao TEXT,
  cliente_id INTEGER,
  PRIMARY KEY(divida_id)
) ;